package com.example.nav_bar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDialogFragment
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {

    private val homeFragment = com.example.nav_bar.fragments.homeFragment
    private val searchFragment = com.example.nav_bar.fragments.searchFragment
    private val notificationsFragment = com.example.nav_bar.fragments.notificationsFragment
    private val settingsFragment = com.example.nav_bar.fragments.settingsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    private fun replaceFragment(fragment: Fragment){
        if (fragment !=null){
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.fragment_container, fragment)
            transaction.commit()
        }
    }
}